## PrayerTimes-Uyghur



## ناماز ۋاقىتلىرى


شىنجاڭ ئۇيغۇر ئاپتونۇم رايۇنىدىكى بىر قىسىم شەھەرلەرنىڭ ناماز ۋاقىتلىرىنى ئاپتوماتىك كۆرسىتىش تور بېتى.

## كۆرۈنمە ئادرىسى(Demo)
[sarwan.github.io/PrayerTimes-Uyghur/](http://sarwan.github.io/PrayerTimes-Uyghur/)


ئىشلىتىلگەن تېخنىكىلار:

* [jquery](http://jquery.com)
* [Bootstrap](http://getbootstrap.com/)
* [bootstrap-uyghur](https://github.com/Sarwan/bootstrap-uyghur)
* [JSON](http://json.org)
* [ug_shaper](https://github.com/finalfantasia/ug_shaper)
